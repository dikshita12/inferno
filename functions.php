<?php
add_action('wp_enqueue_scripts', 'inferno_removeScripts' , 20);
function inferno_removeScripts() {
	
$parent_style = 'parent-style';
wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
  
//De-Queuing Styles sheet 
wp_dequeue_style( 'default',get_template_directory_uri() .'/css/default.css'); 
//EN-Queing Style sheet 
wp_enqueue_style('wlorange', get_stylesheet_directory_uri() . '/wl-orange.css');
}?>
<?php
function inferno_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'after_setup_theme', 'inferno_add_editor_styles' );