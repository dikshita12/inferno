=== Inferno ===
Contributors: weblizar
Tags: two-columns, three-columns, four-columns, custom-menu, right-sidebar, custom-background, featured-image-header, sticky-post, theme-options, threaded-comments, featured-images, flexible-header, translation-ready
Requires at least: 4.0
Tested up to: 5.5
Stable tag: 2.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

= Description =
Inferno is a simple e-commerce theme targeted for offering deep integration with WooCommerce. It is built on solid foundations based on the popular guidelines of WordPress platform. It is the perfect starting point from which to customize your store to match your brand or boosting your organization with an interactive design.

CHANGELOG: 
@Version: 2.0
* Update according to latest enigma
* Update Screenshot

@Version: 1.10[16/01/2020]
*** Enigma V6.0.1 & above Competible.

@Version: 1.09[21/11/2019]
*** BS 4 Competible

@Version: 1.08[26/03/2019]
* Minor Changes.

@Version: 1.07[03/09/2018]
* header image issue fixed
* box layout issue fixed
* search box added
* logo issue fixed
* screenshot updated

@Version: 1.06[21/02/2018]
* Minor Changes.

@Version: 1.05[27/04/2017]
-> Add Product Page Template.
-> Woocommerce Support Added.

CHANGELOG: 
@Version: 1.03[25/02/2015]
Issue FIXED.

@Version: 1.01[14/02/2015]
Issue FIXED.

== Image Resources ==
= Screenshot =
* https://pxhere.com/en/photo/1203027 | CC0 Public Domain
All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)